// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadFormatterSubsystem.h"
#include "Common/CommonStructs.h"
#include "Contracts/SquadFormatterInterface.h"
#include "SquadFormatters/SquareSquadFormatter.h"

TScriptInterface<ISquadFormatterInterface> USquadFormatterSubsystem::GetFormatterObject(
	const ESquadFormatterType& InSquadFormatterType, AActor* InInitiatorOwner)
{
	switch (InSquadFormatterType)
	{
	case ESquadFormatterType::Square:
		{
			if (USquareSquadFormatter* NewFormatter = NewObject<USquareSquadFormatter>())
			{
				NewFormatter->SetInitialOwner(InInitiatorOwner);
				
				TScriptInterface<ISquadFormatterInterface> NewFormatterObject;
				NewFormatterObject.SetObject(NewFormatter);
				NewFormatterObject.SetInterface(NewFormatter);

				return NewFormatterObject;
			}
		}
		break;
		
	case ESquadFormatterType::Triangle: break;
	case ESquadFormatterType::Rhombus: break;
	case ESquadFormatterType::Rectangle: break;
	}

	return nullptr;
}
