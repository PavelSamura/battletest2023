// Fill out your copyright notice in the Description page of Project Settings.

#include "Common/CommonStructs.h"

FCurrentFormatter::FCurrentFormatter(): FormatterType(ESquadFormatterType::None)
{
}

FCurrentFormatter::FCurrentFormatter(
	const ESquadFormatterType& InSquadFormatterType,
	const TScriptInterface<ISquadFormatterInterface>& InSquadFormatterObject)
: FormatterType(InSquadFormatterType), InSquadFormatterObject(InSquadFormatterObject)
{
}

bool FCurrentFormatter::IsValidFormatter() const
{
	return FormatterType != ESquadFormatterType::None && IsValid(FormatterObject.GetObject());
}

void FCurrentFormatter::Clear()
{
	FormatterType = ESquadFormatterType::None;
	FormatterObject = nullptr;
}
