// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadControllers/SquadController.h"
#include "BattleGameModeBase.h"
#include "SquadFormatterSubsystem.h"
#include "Kismet/GameplayStatics.h"
#include "Subsystems/SubsystemBlueprintLibrary.h"

ASquadController::ASquadController()
{
	PrimaryActorTick.bCanEverTick = true;

	NumberUnitsInSquad = 60;
	AttackDistance = 300.0f;
	SquadMovementSpeed = 350.0f;
	SquadFormatterType = ESquadFormatterType::Square;

	TargetObjectPtr = nullptr;
	SquadFormatterSubsystemPtr = nullptr;
}

void ASquadController::SetTarget(AActor* InTargetActor)
{
	if (IsValid(InTargetActor) && InTargetActor != TargetObjectPtr)
	{
		TargetObjectPtr = InTargetActor;
	}
}

void ASquadController::StartAttack()
{
	K2_BattleStartAction();
	
	for (const auto& CurrentUnit : Units)
	{
		CurrentUnit->StartAttack();
	}
	
	//smth...
}

AActor* ASquadController::GetTarget() const
{
	return TargetObjectPtr.Get();
}

void ASquadController::SetNewSquadFormatter(const ESquadFormatterType& InNewSquadFormatterType)
{
	if (InNewSquadFormatterType != ESquadFormatterType::None &&
		InNewSquadFormatterType != SquadFormatterType &&
		SquadFormatterSubsystemPtr.IsValid())
	{
		SquadFormatterType = InNewSquadFormatterType;
		CurrentFormatter = SquadFormatterSubsystemPtr->GetFormatterObject(SquadFormatterType, this);

		SquadFormatterInitialize();
	}
}

void ASquadController::SquadFormatterInitialize()
{
	if (UGameInstanceSubsystem* GameInstanceSubsystem =
		USubsystemBlueprintLibrary::GetGameInstanceSubsystem(this, USquadFormatterSubsystem::StaticClass()))
	{
		if (USquadFormatterSubsystem* SquadFormatterSubsystem = Cast<USquadFormatterSubsystem>(GameInstanceSubsystem))
		{
			SquadFormatterSubsystemPtr = TWeakObjectPtr<USquadFormatterSubsystem>(SquadFormatterSubsystem);
			CurrentFormatter = SquadFormatterSubsystem->GetFormatterObject(SquadFormatterType, this);
		}
	}
	
	if (IsValid(CurrentFormatter.GetObject()))
	{
		FOnExecuteResult OnExecuteResult;
		OnExecuteResult.BindDynamic(this, &ASquadController::FormatterExecuteCallback);

		CurrentFormatter.GetInterface()->Execute(OnExecuteResult);
	}
}

void ASquadController::FormatterExecuteCallback(bool InExecuteResult)
{
	//Formatter executed reaction...
}

void ASquadController::SquadRegister()
{
	AGameModeBase* GameModeBase = UGameplayStatics::GetGameMode(this);
	if (ABattleGameModeBase* BattleGameModeBase = Cast<ABattleGameModeBase>(GameModeBase))
	{
		BattleGameModeBase->SquadRegistration(this);
	}
}

void ASquadController::AddNewUnit(ASquadController* NewUnit)
{
	if (IsValid(NewUnit))
	{
		Units.Add(NewUnit);
	}
}
