// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadFormatters/SquadFormatterBase.h"

void USquadFormatterBase::SetInitialOwner(AActor* InInitialOwner)
{
	if (IsValid(InInitialOwner))
	{
		OwnerActorPtr = TWeakObjectPtr<AActor>(InInitialOwner);
	}
}

void USquadFormatterBase::Execute(FOnExecuteResult& ExecuteResultCallback)
{
	ExecuteResultCallback.ExecuteIfBound(false);
}
