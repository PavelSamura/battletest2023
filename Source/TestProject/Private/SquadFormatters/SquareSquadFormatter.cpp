// Fill out your copyright notice in the Description page of Project Settings.

#include "SquadFormatters/SquareSquadFormatter.h"
#include "SquadControllers/SquadController.h"

void USquareSquadFormatter::Execute(FOnExecuteResult& ExecuteResultCallback)
{
	if (!OwnerActorPtr.IsValid())
	{
		Super::Execute(ExecuteResultCallback);
		return;
	}
	
	if (ASquadController* SquadController = Cast<ASquadController>(OwnerActorPtr.Get()))
	{
		UWorld* World = SquadController->GetWorld();
		if (!IsValid(World))
		{
			Super::Execute(ExecuteResultCallback);
			return;
		}

		FTransform LastUnitTransform = SquadController->GetActorTransform();
		for (int32 UnitIndex = 0; UnitIndex < SquadController->NumberUnitsInSquad; ++UnitIndex)
		{
			constexpr int32 FormatterDelimiter = 10;
			constexpr float DistanceBetweenUnits = 200.0f;
			UClass* SquadControllerClass = SquadController->GetClass();
			ASquadController* NewSquadUnitActor = World->SpawnActor<ASquadController>(SquadControllerClass);
			
			const int32 CurrentRowIndex = UnitIndex / FormatterDelimiter;
			const int32 CurrentColumnIndex = UnitIndex % FormatterDelimiter;
			
			LastUnitTransform.SetTranslation(SquadController->GetActorLocation() +
				FVector(CurrentRowIndex * DistanceBetweenUnits, CurrentColumnIndex * DistanceBetweenUnits, 0.0f));
			NewSquadUnitActor->SetActorTransform(LastUnitTransform);
			
			NewSquadUnitActor->SetTarget(SquadController->GetTarget());
			SquadController->AddNewUnit(NewSquadUnitActor);
		}
	}

	ExecuteResultCallback.ExecuteIfBound(true);
}
