// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleController.h"
#include "BattleGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "SquadControllers/SquadController.h"

constexpr float ABattleController::FirstSquadStartDuration = 3.0f;
constexpr float ABattleController::SecondSquadStartDuration = 30.0f;

ABattleController::ABattleController()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABattleController::SetNewTargetActor(AActor* InNewTargetActor)
{
	if (IsValid(InNewTargetActor) && InNewTargetActor != CurrentTargetActor)
	{
		CurrentTargetActor = InNewTargetActor;
	}
}

void ABattleController::Initialize()
{
	const UWorld* World = GetWorld();
	if (!IsValid(World))
	{
		return;
	}
	
	if (BattleGameModePtr.IsValid())
	{
		BattleGameModePtr->GetSquadControllers(SquadControllers);
	}

	if (SquadControllers.IsEmpty())
	{
		return;
	}

	for (const auto& SquadController : SquadControllers)
	{
		if (ASquadController* CurrentSquadController = Cast<ASquadController>(SquadController))
		{
			SetTargetForSquad(CurrentSquadController);
		}
	}
	
	FTimerHandle FirstTimerHandle;
	World->GetTimerManager().SetTimer(FirstTimerHandle, FTimerDelegate::CreateWeakLambda(this,
	[this]
	{
		if (SquadControllers.Num() > 0)
		{
			if (ASquadController* SquareController = Cast<ASquadController>(SquadControllers[0]))
			{
				SquareController->StartAttack();
			}
		}
	}), FirstSquadStartDuration, false);

	FTimerHandle SecondTimerHandle;
	World->GetTimerManager().SetTimer(SecondTimerHandle, FTimerDelegate::CreateWeakLambda(this,
	[this]
	{
		if (SquadControllers.Num() > 1)
		{
			if (ASquadController* SquareController = Cast<ASquadController>(SquadControllers[1]))
			{
				SquareController->StartAttack();
			}
		}
	}), SecondSquadStartDuration, false);
}

void ABattleController::SetTargetForSquad(ASquadController* InSquadController) const
{
	if (IsValid(InSquadController))
	{
		InSquadController->SetTarget(CurrentTargetActor);
		InSquadController->SquadFormatterInitialize();
	}
}

TSubclassOf<AActor> ABattleController::GetEnemyClass() const
{
	return CurrentTargetActorClass;
}

AActor* ABattleController::GetTargetActor() const
{
	return CurrentTargetActor;
}

void ABattleController::BeginPlay()
{
	Super::BeginPlay();

	AGameModeBase* GameModeBase = UGameplayStatics::GetGameMode(this);
	if (ABattleGameModeBase* BattleGameModeBase = Cast<ABattleGameModeBase>(GameModeBase))
	{
		BattleGameModePtr = TWeakObjectPtr<ABattleGameModeBase>(BattleGameModeBase);
		BattleGameModePtr->BattleControllerRegistration(this);
	}
}
