// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleGameModeBase.h"
#include "BattleController.h"
#include "Kismet/GameplayStatics.h"
#include "SquadControllers/SquadController.h"

constexpr float ABattleGameModeBase::MathTimeDuration = 120.0f;

ABattleGameModeBase::ABattleGameModeBase()
{
	CurrentMathType = EMatchType::None;
}

void ABattleGameModeBase::SquadRegistration(ASquadController* InSquadController)
{
	if (IsValid(InSquadController))
	{
		SquadControllers.Add(InSquadController);
		InSquadController->SetTarget(BattleControllerPtr->GetTargetActor());
	}
}

void ABattleGameModeBase::BattleControllerRegistration(ABattleController* InBattleController)
{
	if (IsValid(InBattleController))
	{
		BattleControllerPtr = TWeakObjectPtr<ABattleController>(InBattleController);

		if (!GetWorld())
		{
			return;
		}

		UGameplayStatics::GetAllActorsOfClass(this, ASquadController::StaticClass(), SquadControllers);
		
		if (AActor* EnemyActor = GetWorld()->SpawnActor(BattleControllerPtr->GetEnemyClass()))
		{
			BattleControllerPtr->SetNewTargetActor(EnemyActor);
			BattleControllerPtr->Initialize();
			
			SetMathType(EMatchType::Start);

			FTimerHandle TimerHandle;
			GetWorld()->GetTimerManager().SetTimer(TimerHandle, FTimerDelegate::CreateWeakLambda(this,
			[this]
			{
				SetMathType(EMatchType::Stop);
			}), MathTimeDuration, false);
		}
	}
}

void ABattleGameModeBase::SetMathType(const EMatchType& InMathType)
{
	CurrentMathType = InMathType;
	OnMatchStateChanged.Broadcast(CurrentMathType);
}

EMatchType ABattleGameModeBase::GetMatchType() const
{
	return CurrentMathType;
}

void ABattleGameModeBase::GetSquadControllers(TArray<AActor*>& OutSquadControllers) const
{
	OutSquadControllers = SquadControllers;
}
