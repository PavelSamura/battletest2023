// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SquadFormatterSubsystem.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"

#include "SquadController.generated.h"

class ISquadFormatterInterface;
enum class ESquadFormatterType : uint8;

UCLASS()
class TESTPROJECT_API ASquadController : public ACharacter
{
	GENERATED_BODY()
	
public:	

	ASquadController();

	UFUNCTION(BlueprintCallable)
	void SquadFormatterInitialize();

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* InTargetActor);

	UFUNCTION(BlueprintCallable)
	void StartAttack();
	
	UFUNCTION(BlueprintCallable)
	AActor* GetTarget() const;

	UFUNCTION(BlueprintCallable)
	void SetNewSquadFormatter(const ESquadFormatterType& InNewSquadFormatterType);

	UFUNCTION(BlueprintCallable)
	void SquadRegister();

	UFUNCTION(BlueprintImplementableEvent)
	void K2_BattleStartAction();

	UFUNCTION(BlueprintCallable)
	void AddNewUnit(ASquadController* NewUnit);
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumberUnitsInSquad;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ESquadFormatterType SquadFormatterType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AttackDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SquadMovementSpeed;

private:
	
	UPROPERTY()
	TScriptInterface<ISquadFormatterInterface> CurrentFormatter;

	TWeakObjectPtr<USquadFormatterSubsystem> SquadFormatterSubsystemPtr;
	TWeakObjectPtr<AActor> TargetObjectPtr;

private:

	UFUNCTION()
	void FormatterExecuteCallback(bool InExecuteResult);

	UPROPERTY()
	TArray<ASquadController*> Units;
};
