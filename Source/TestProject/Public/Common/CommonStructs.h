// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Contracts/SquadFormatterInterface.h"

#include "CommonStructs.generated.h"

UENUM(BlueprintType)
enum class ESquadFormatterType : uint8
{
	None UMETA(Hidden),
	Square = 1 << 1,
	Triangle = 2 << 1,
	Rhombus = 3 << 1,
	Rectangle = 4 << 1,
	MAX UMETA(Hidden)
};

UENUM(BlueprintType)
enum class EMatchType : uint8
{
	None UMETA(Hidden),
	Start = 1,
	Stop = 2,
	MAX UMETA(Hidden)
};

USTRUCT(BlueprintType)
struct FCurrentFormatter
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	ESquadFormatterType FormatterType;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TScriptInterface<ISquadFormatterInterface> FormatterObject;
	const TScriptInterface<ISquadFormatterInterface> InSquadFormatterObject;

public:
	
	FCurrentFormatter();
	FCurrentFormatter(
		const ESquadFormatterType& InSquadFormatterType,
		const TScriptInterface<ISquadFormatterInterface>& InSquadFormatterObject);
	
	bool IsValidFormatter() const;
	void Clear();

	FCurrentFormatter operator=(const FCurrentFormatter& Right)
	{
		this->FormatterType = Right.FormatterType;
		this->FormatterObject = Right.FormatterObject;

		return *this;
	}
};
