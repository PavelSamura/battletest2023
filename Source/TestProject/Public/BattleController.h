// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BattleController.generated.h"

class ABattleGameModeBase;
class ASquadController;

UCLASS()
class TESTPROJECT_API ABattleController : public AActor
{
	GENERATED_BODY()
	
public:	

	ABattleController();

	UFUNCTION(BlueprintCallable)
	void SetNewTargetActor(AActor* InNewTargetActor);

	UFUNCTION(BlueprintCallable)
	void Initialize();

	UFUNCTION(BlueprintCallable)
	TSubclassOf<AActor> GetEnemyClass() const;

	UFUNCTION(BlueprintCallable)
	AActor* GetTargetActor() const;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> CurrentTargetActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* CurrentTargetActor;
	
protected:
	
	virtual void BeginPlay() override;
	void SetTargetForSquad(ASquadController* InSquadController) const;

private:

	UPROPERTY()
	TArray<AActor*> SquadControllers;
	
	TWeakObjectPtr<ABattleGameModeBase> BattleGameModePtr;

	static const float FirstSquadStartDuration;
	static const float SecondSquadStartDuration;
};
