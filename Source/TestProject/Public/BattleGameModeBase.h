// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "BattleGameModeBase.generated.h"

enum class EMatchType : uint8;
class ABattleController;
class ASquadController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMatchStateChanged, EMatchType, Value);

UCLASS()
class TESTPROJECT_API ABattleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	ABattleGameModeBase();
	
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnMatchStateChanged OnMatchStateChanged;
	
	UFUNCTION(BlueprintCallable)
	void SquadRegistration(ASquadController* InSquadController);

	UFUNCTION(BlueprintCallable)
	void BattleControllerRegistration(ABattleController* InBattleController);

	UFUNCTION(BlueprintCallable)
	void SetMathType(const EMatchType& InMathType);
	
	UFUNCTION(BlueprintCallable)
	EMatchType GetMatchType() const;
	
	void GetSquadControllers(TArray<AActor*>& OutSquadControllers) const;

private:

	UPROPERTY()
	TArray<AActor*> SquadControllers;
	
	TWeakObjectPtr<ABattleController> BattleControllerPtr;

	static const float MathTimeDuration;
	EMatchType CurrentMathType;
};
