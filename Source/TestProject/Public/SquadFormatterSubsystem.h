// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Common/CommonStructs.h"
#include "Subsystems/GameInstanceSubsystem.h"

#include "SquadFormatterSubsystem.generated.h"

class ISquadFormatterInterface;
enum class ESquadFormatterType : uint8;

UCLASS()
class TESTPROJECT_API USquadFormatterSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	TScriptInterface<ISquadFormatterInterface> GetFormatterObject(
		const ESquadFormatterType& InSquadFormatterType, AActor* InInitiatorOwner);
};
