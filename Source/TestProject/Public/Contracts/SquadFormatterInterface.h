// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SquadFormatterInterface.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnExecuteResult, bool, Value);

UINTERFACE()
class TESTPROJECT_API USquadFormatterInterface : public UInterface
{
	GENERATED_BODY()
};

class TESTPROJECT_API ISquadFormatterInterface
{
	GENERATED_BODY()

public:

	virtual void SetInitialOwner(AActor* InInitialOwner) = 0;
	virtual void Execute(FOnExecuteResult& ExecuteResultCallback) = 0;
};
