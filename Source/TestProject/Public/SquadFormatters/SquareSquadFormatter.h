// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SquadFormatters/SquadFormatterBase.h"

#include "SquareSquadFormatter.generated.h"

UCLASS()
class TESTPROJECT_API USquareSquadFormatter : public USquadFormatterBase
{
	GENERATED_BODY()

public:

	virtual void Execute(FOnExecuteResult& ExecuteResultCallback) override;
};
