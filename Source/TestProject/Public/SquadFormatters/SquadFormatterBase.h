// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Contracts/SquadFormatterInterface.h"

#include "SquadFormatterBase.generated.h"

UCLASS()
class TESTPROJECT_API USquadFormatterBase : public UObject, public ISquadFormatterInterface
{
	GENERATED_BODY()

public:
	
	// ISquadFormatterInterface implements
	virtual void SetInitialOwner(AActor* InInitialOwner) override;
	virtual void Execute(FOnExecuteResult& ExecuteResultCallback) override;
	// ~

protected:

	TWeakObjectPtr<AActor> OwnerActorPtr;
};
